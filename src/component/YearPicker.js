import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';

class YearPicker extends React.Component {
  constructor() {
    super();
    this.state = {
      isFocus: false,
      value: null,
    };
    this.data = [
      {label: '2019', value: '1'},
      {label: '2020', value: '2'},
      {label: '2021', value: '3'},
      {label: '2022', value: '4'},
      {label: '2023', value: '5'},
    ];
  }

  render() {
    return (
      <View>
        <Dropdown
          style={[styles.dropdown, this.state.isFocus && {borderColor: 'blue'}]}
          placeholderStyle={styles.placeholderStyle}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          iconStyle={styles.iconStyle}
          data={this.data}
          // search
          maxHeight={100}
          labelField="label"
          valueField="value"
          placeholder={!this.state.isFocus ? 'Year' : '...'}
          searchPlaceholder="Search..."
          value={this.state.value}
          onFocus={() => this.setState({isFocus: true})}
          onBlur={() => this.setState({isFocus: false})}
          onChange={item => {
            this.setState({value: item.value});
            this.setState({isFocus: false});
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dropdown: {
    margin: 5,
    height: 30,
    width: 80,
    backgroundColor: 'white',
    borderRadius: 12,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 8,
  },
  placeholderStyle: {
    fontSize: 10,
  },
  selectedTextStyle: {
    fontSize: 10,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 10,
  },
});

export default YearPicker;
