import 'react-native-gesture-handler';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MainScreen from '../navigation/mainScreen';
import {useContext} from 'react';
import {AuthContext} from './AuthContext';
import Login from '../navigation/screens/login';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createNativeStackNavigator();

const RootContext = () => {
  const {authState} = useContext(AuthContext);
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {authState.isSignIn ? (
          <Stack.Screen name="Main" component={MainScreen} />
        ) : (
          <Stack.Screen name="Login" component={Login} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootContext;
