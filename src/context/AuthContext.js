import {createContext, useState} from 'react';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
  const [auth, setAuth] = useState({
    isSignIn: false,
    email: '',
  });

  const signIn = ({email}) => {
    setAuth({
      isSignIn: true,
      email,
    });
  };

  const signOut = () => {
    setAuth({
      isSignOut: false,
      email: '',
    });
  };

  return (
    <AuthContext.Provider value={{signIn, signOut, authState: auth}}>
      {children}
    </AuthContext.Provider>
  );
};
