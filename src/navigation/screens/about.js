import * as React from 'react';

import {View, Text, StyleSheet, Image, FlatList} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Data} from './dummy';
class AboutScreen extends React.Component {
  constructor({navigation}) {
    super();
    this.navigation = navigation;
    this.month = new Date();
    this.state = {
      name: 'Anonymous',
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View
            style={{
              marginLeft: 21,
              alignItems: 'center',
            }}>
            <Image
              style={{marginTop: 20, height: 70, width: 70}}
              source={require('../../assets/deddy.png')}
            />
          </View>
          <View style={{marginLeft: 15}}>
            <Text style={styles.title}>Welcome, {this.state.name}</Text>
            <Text>
              {this.month.toLocaleDateString('default', {
                month: 'long',
                day: '2-digit',
                year: 'numeric',
              })}
            </Text>
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.contentOne}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                marginLeft: 21,
              }}>
              <Text style={styles.text}>Name</Text>
              <Text style={styles.text}>Handphone</Text>
              <Text style={styles.text}>Email</Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                marginLeft: 31,
              }}>
              <Text style={styles.text}>contoh nama</Text>
              <Text style={styles.text}>081212121822</Text>
              <Text style={styles.text}>contoh@gmail.com</Text>
            </View>
          </View>

          <View style={styles.contentTwo}>
            <SafeAreaView style={{flexDirection: 'column'}}>
              <FlatList
                // horizontal
                data={Data}
                keyExtractor={item => item.id}
                renderItem={({item}) => {
                  return (
                    <>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginBottom: 10,
                          marginLeft: 20,
                          marginTop: 10,
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <View>
                          <Image
                            source={item.image}
                            style={{height: 38, width: 30}}
                          />
                          <View style={{marginTop: 5}}>
                            <Text>{item.name}</Text>
                          </View>
                        </View>
                        <View style={{marginRight: 40}}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text>{item.level * 100}%</Text>
                            <Text>Lv</Text>
                          </View>
                          <View
                            style={{
                              width: 140 * item.level,
                              height: 5,
                              backgroundColor: 'red',
                            }}></View>
                        </View>
                      </View>
                    </>
                  );
                }}
              />
            </SafeAreaView>
          </View>

          <View style={styles.contentThree}>
            <View>
              <Text style={{alignSelf: 'center', marginTop: 10}}>
                Find me here
              </Text>
              {/* github gitlab */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginTop: 10,
                }}>
                <View style={{alignItems: 'center'}}>
                  <Image source={require('../../assets/github.png')} />
                  <Text style={styles.text}>@natannegara</Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Image source={require('../../assets/gitlab.png')} />
                  <Text style={styles.text}>@Natannegara_</Text>
                </View>
              </View>
            </View>
            {/* sosmed */}
            <View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{marginLeft: 35}}>
                  <Image source={require('../../assets/fb.png')} />
                </View>
                <View
                  style={{
                    marginLeft: 25,
                    alignSelf: 'center',
                  }}>
                  <Text style={styles.text}>Muhammad Fitranatanegara</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <View style={{marginLeft: 40}}>
                  <Image source={require('../../assets/twitter.png')} />
                </View>
                <View style={{marginLeft: 30, alignSelf: 'center'}}>
                  <Text style={styles.text}>@Natan</Text>
                </View>
              </View>
              <View
                style={{flexDirection: 'row', marginLeft: 30, marginTop: 10}}>
                <Image source={require('../../assets/ig.png')} />
                <View
                  style={{
                    marginLeft: 20,
                    alignSelf: 'center',
                  }}>
                  <Text style={styles.text}>@Natannegara</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flexDirection: 'column',
    flex: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 110,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
  },
  contentOne: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 110,
    width: 317,
    flexDirection: 'row',
  },
  contentTwo: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 220,
    width: 317,
  },
  contentThree: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 250,
    width: 317,
  },
  text: {
    color: 'black',
  },
  footer: {
    backgroundColor: 'white',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  imageFooter: {
    height: 30,
    width: 30,
  },
  title: {
    fontFamily: 'Poppins-ExtraLight',
    fontWeight: 'bold',
    fontSize: 17,
  },
});

export default AboutScreen;
