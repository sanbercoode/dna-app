const Data = [
  {
    id: 1,
    image: require('../../assets/js.png'),
    name: 'Javascript',
    level: 1,
  },
  {
    id: 2,
    image: require('../../assets/k8s.png'),
    name: 'Kubernetes',
    level: 0.8,
  },
  {
    id: 3,
    image: require('../../assets/react.png'),
    name: 'React',
    level: 0.9,
  },
  {
    id: 4,
    image: require('../../assets/python.png'),
    name: 'Python',
    level: 0.4,
  },
];

export {Data};
