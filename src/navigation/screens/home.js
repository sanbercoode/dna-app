import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Button,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Data} from './dummyHome';

class HomeScreen extends React.Component {
  constructor({navigation}) {
    super();
    this.navigation = navigation;
    // tinggal ganti status paid saja, menggunakan API
    this.paid = true;
    this.month = new Date();
    this.state = {
      backgroundColor: 'red',
      status: 'Unpaid',
      name: 'Anonymous',
    };
    this.saldo = 50000;
    this.rpPlus = '+ Rp. ';
    this.rpMinus = '- Rp. ';
  }
  updatePayment() {
    this.paid == true
      ? this.setState({backgroundColor: 'green', status: 'Paid'})
      : this.setState({backgroundColor: 'red', status: 'Unpaid'});
  }

  componentDidMount() {
    this.updatePayment();
  }

  render() {
    return (
      <View style={styles.container}>
        {/* header */}
        <View style={styles.header}>
          <View
            style={{
              marginLeft: 21,
              alignItems: 'center',
            }}>
            <Image
              style={{marginTop: 20, height: 70, width: 70}}
              source={require('../../assets/deddy.png')}
            />
          </View>
          <View style={{marginLeft: 15}}>
            <Text style={styles.title}>Welcome, {this.state.name}</Text>
            <Text>
              {this.month.toLocaleDateString('default', {
                month: 'long',
                day: '2-digit',
                year: 'numeric',
              })}
            </Text>
          </View>
        </View>
        {/* content */}
        <View style={styles.content}>
          <View style={styles.contentOne}>
            <View style={{paddingLeft: 17}}>
              <Text style={styles.title}>Total Saldo Club</Text>
            </View>
            <View
              style={{
                height: 42,
                width: 140,
                borderColor: '#CEC410',
                borderWidth: 1,
                justifyContent: 'center',
                borderRadius: 12,
                backgroundColor: '#ffffff',
                alignItems: 'center',
                marginLeft: 35,
              }}>
              <Text>Rp. {this.saldo}</Text>
            </View>
          </View>

          <View style={styles.contentTwo}>
            <Text style={[styles.title, {paddingLeft: 17}]}>
              Your Payment Status
            </Text>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 12,
                paddingTop: 18,
                backgroundColor: '#ffffff',
                borderColor: '#CEC410',
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text
                  style={{
                    paddingLeft: 17,
                    fontFamily: 'Poppins-ExtraLight',
                    fontWeight: 'bold',
                  }}>
                  Status
                </Text>
                <View
                  style={{
                    marginRight: 29,
                    width: 111,
                    height: 21,
                    alignItems: 'center',
                    borderRadius: 12,
                    backgroundColor: this.state.backgroundColor,
                  }}>
                  <Text style={{color: '#ffffff'}}>{this.state.status}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingTop: 11,
                  paddingBottom: 11,
                }}>
                <Text
                  style={{
                    paddingLeft: 17,
                    fontFamily: 'Poppins-ExtraLight',
                    fontWeight: 'bold',
                  }}>
                  Month
                </Text>
                <View
                  style={{
                    marginRight: 29,
                    width: 111,
                    height: 21,
                    alignItems: 'center',
                    borderRadius: 12,
                    backgroundColor: '#f0f0f0',
                  }}>
                  <Text>
                    {this.month.toLocaleDateString('default', {month: 'long'})}
                  </Text>
                </View>
              </View>
            </View>
          </View>

          <View>
            <Text style={[styles.title, {paddingLeft: 17}]}>
              Club Transaction
            </Text>
            <View style={styles.contentThree}>
              <SafeAreaView>
                <FlatList
                  style={{marginTop: 21, marginLeft: 7, marginRight: 7}}
                  data={Data}
                  keyExtractor={item => item.id}
                  renderItem={({item}) => {
                    return (
                      <View
                        style={[
                          styles.transactionType,
                          item.transactionType === 'Received'
                            ? styles.received
                            : styles.sentout,
                        ]}>
                        <View style={{flexDirection: 'row'}}>
                          <Image
                            source={
                              item.transactionType === 'Received'
                                ? require('../../assets/received.png')
                                : require('../../assets/sentout.png')
                            }
                            style={{
                              height: 20,
                              width: 20,
                              marginLeft: 6,
                              marginRight: 7,
                            }}
                          />
                          <View>
                            <Text style={{fontSize: 11}}>
                              {item.transactionType}
                            </Text>
                            <Text style={{fontSize: 9}}>{item.date}</Text>
                          </View>
                        </View>
                        <View style={{paddingRight: 105}}>
                          <Text>
                            {item.transactionType === 'Received'
                              ? this.rpPlus
                              : this.rpMinus}
                            {item.nominal}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                />
              </SafeAreaView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flexDirection: 'column',
    flex: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 110,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
  },
  contentOne: {
    borderRadius: 10,
    height: 110,
    width: 317,
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentTwo: {
    borderRadius: 10,
    width: 317,
  },
  contentThree: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 250,
    width: 317,
  },
  transactionType: {
    borderRadius: 12,
    height: 32,
    width: 307,
    marginBottom: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  received: {
    backgroundColor: '#EFFFF3',
  },
  sentout: {
    backgroundColor: '#FFFEE7',
  },
  title: {
    fontFamily: 'Poppins-ExtraLight',
    fontWeight: 'bold',
    fontSize: 17,
  },
});

export default HomeScreen;
