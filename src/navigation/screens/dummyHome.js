const Data = [
  {
    id: 1,
    nominal: 50000,
    transactionType: 'Received',
    date: '22-03-2022',
  },
  {
    id: 2,
    nominal: 10000,
    transactionType: 'Sent Out',
    date: '26-02-2022',
  },
  {
    id: 3,
    nominal: 35000,
    transactionType: 'Sent Out',
    date: '08-01-2022',
  },
  {
    id: 4,
    nominal: 75000,
    transactionType: 'Received',
    date: '22-12-2021',
  },
  {
    id: 5,
    nominal: 50000,
    transactionType: 'Received',
    date: '20-12-2021',
  },
  {
    id: 6,
    nominal: 50000,
    transactionType: 'Received',
    date: '15-12-2021',
  },
  {
    id: 7,
    nominal: 10000,
    transactionType: 'Sent Out',
    date: '10-12-2021',
  },
  {
    id: 8,
    nominal: 3000,
    transactionType: 'Sent Out',
    date: '10-12-2021',
  },
  {
    id: 9,
    nominal: 6000,
    transactionType: 'Sent Out',
    date: '10-12-2021',
  },
  {
    id: 10,
    nominal: 200,
    transactionType: 'Sent Out',
    date: '10-12-2021',
  },
  {
    id: 11,
    nominal: 120000,
    transactionType: 'Sent Out',
    date: '10-12-2021',
  },
];

export {Data};
