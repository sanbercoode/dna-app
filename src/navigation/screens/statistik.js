import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  Button,
} from 'react-native';
import {LineChart} from 'react-native-chart-kit';
import {Dropdown} from 'react-native-element-dropdown';
import MonthPicker from '../../component/MonthPicker';
import YearPicker from '../../component/YearPicker';
MonthPicker;
class StatistikScreen extends React.Component {
  constructor({navigation}) {
    super();
    this.navigation = navigation;
    // tinggal ganti status paid saja, menggunakan API
    this.paid = true;
    this.month = new Date();
    this.state = {
      name: 'Anonymous',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        {/* header */}
        <View style={styles.header}>
          <View
            style={{
              marginLeft: 21,
              alignItems: 'center',
            }}>
            <Image
              style={{marginTop: 20, height: 70, width: 70}}
              source={require('../../assets/deddy.png')}
            />
          </View>
          <View style={{marginLeft: 15}}>
            <Text style={styles.title}>Welcome, {this.state.name}</Text>
            <Text>
              {this.month.toLocaleDateString('default', {
                month: 'long',
                day: '2-digit',
                year: 'numeric',
              })}
            </Text>
          </View>
        </View>
        {/* content */}
        <View style={styles.content}>
          <View style={styles.contentOne}>
            {/* part left */}
            <View style={{flexDirection: 'column', marginLeft: 17}}>
              <View>
                <Text style={[styles.title]}>Best Score</Text>
              </View>
              <View>
                <Text style={[styles.title, {fontSize: 9}]}>
                  Accumulation Scoring Point
                </Text>
              </View>
            </View>
            {/* part right */}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View>
                <Image
                  style={{height: 20, width: 20, marginRight: 16}}
                  source={require('../../assets/medal.png')}
                />
              </View>
              <View>
                <View>
                  <Text style={[styles.title, {fontSize: 9}]}>
                    Congratulation
                  </Text>
                </View>
                <View>
                  <Text style={styles.title}>Paijo 566</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.contentTwo}>
            <Text style={[styles.title, {paddingLeft: 17}]}>
              Member Score Total
            </Text>
            <View
              style={{
                height: 62,
                width: 317,
                backgroundColor: '#ffffff',
                borderRadius: 12,
                borderColor: '#CEC410',
                borderWidth: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{justifyContent: 'space-around', marginLeft: 17}}>
                <Text style={[styles.title, {fontSize: 14}]}>Slamet</Text>
                <Text style={[styles.title, {fontSize: 14}]}>Month</Text>
              </View>
              <View
                style={{
                  justifyContent: 'space-around',
                }}>
                <View
                  style={{
                    marginRight: 22,
                    alignItems: 'center',
                    backgroundColor: 'black',
                    borderRadius: 12,
                  }}>
                  <Text
                    style={[styles.title, {color: '#ffffff', fontSize: 14}]}>
                    250
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginRight: 22,
                  }}>
                  <View
                    style={{
                      width: 80,
                      backgroundColor: '#DEDEDE',
                      borderRadius: 12,
                      marginRight: 6,
                      alignItems: 'center',
                    }}>
                    <Text>June</Text>
                  </View>
                  <View
                    style={{
                      width: 80,
                      backgroundColor: '#DEDEDE',
                      borderRadius: 12,
                      alignItems: 'center',
                    }}>
                    <Text>2020</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View style={{alignItems: 'center'}}>
            <Text style={[styles.title]}>Statistic</Text>
            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text>From</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: 10,
                    }}>
                    <MonthPicker />
                    <YearPicker />
                  </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text>To</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: 28,
                    }}>
                    <MonthPicker />
                    <YearPicker />
                  </View>
                </View>
              </View>
              <View
                style={{
                  height: 35,
                  borderRadius: 12,
                  alignSelf: 'center',
                  marginLeft: 15,
                }}>
                <Button title="Proceed" />
              </View>
            </View>
            <View
              style={[
                styles.contentThree,
                {alignItems: 'center', justifyContent: 'center'},
              ]}>
              <LineChart
                data={{
                  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June'],
                  datasets: [
                    {
                      data: [
                        Math.random() * 100,
                        Math.random() * 100,
                        Math.random() * 100,
                        Math.random() * 100,
                        Math.random() * 100,
                        Math.random() * 100,
                      ],
                    },
                  ],
                }}
                // width={Dimensions.get('window').width} // from react-native
                width={300}
                height={220}
                yAxisLabel="$"
                yAxisSuffix="k"
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                  backgroundColor: '#e26a00',
                  backgroundGradientFrom: '#fb8c00',
                  backgroundGradientTo: '#ffa726',
                  decimalPlaces: 2, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) =>
                    `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 16,
                  },
                  propsForDots: {
                    r: '6',
                    strokeWidth: '2',
                    stroke: '#ffa726',
                  },
                }}
                bezier
                style={{
                  marginVertical: 8,
                  borderRadius: 16,
                }}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flexDirection: 'column',
    flex: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 110,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
  },
  contentOne: {
    borderRadius: 10,
    height: 110,
    width: 317,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentTwo: {
    borderRadius: 10,
    width: 317,
  },
  contentThree: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 250,
    width: 317,
  },
  title: {
    fontFamily: 'Poppins-ExtraLight',
    fontWeight: 'bold',
    fontSize: 17,
  },
  //
  dropdown: {
    margin: 16,
    height: 50,
    width: 100,
    backgroundColor: 'white',
    borderRadius: 12,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
});

export default StatistikScreen;
