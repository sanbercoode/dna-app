import React, {useState, useContext} from 'react';
import {View, Text, StyleSheet, Image, TextInput, Button} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// tambahan auth
import {AuthContext} from '../../context/AuthContext';

export default function Login() {
  const {signIn, authState, ...others} = useContext(AuthContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {/* logo part */}
        <View style={styles.logo}>
          <Image
            style={{height: 187, width: 187}}
            source={require('../../assets/dna.png')}
          />
        </View>
        {/* app name part*/}
        <View style={styles.appName}>
          <Text
            style={{
              fontSize: 24,
              fontFamily: 'Oswald',
              color: 'white',
            }}>
            Your fascinating scoring app
          </Text>
          <Text style={{fontFamily: 'PoppinsExtraLight', color: 'white'}}>
            provided by DNA Club to improve
          </Text>
          <Text style={{fontFamily: 'PoppinsExtraLight', color: 'white'}}>
            and maintain your database
          </Text>
        </View>
        {/* input text part */}
        <View style={styles.login}>
          <View style={styles.email}>
            <View>
              <Image
                style={{height: 17, width: 19, marginLeft: 14, marginTop: 15}}
                source={require('../../assets/email.png')}
              />
            </View>
            <View
              style={{marginLeft: 9, marginTop: 8, flexDirection: 'column'}}>
              <Text
                style={{
                  fontSize: 10,
                  color: 'white',
                }}>
                Email
              </Text>
              <TextInput
                style={styles.inputText}
                onChangeText={setEmail}
                placeholder="your@email.com"
                value={email}
              />
            </View>
          </View>

          <View style={styles.password}>
            <View>
              <Image
                style={{height: 17, width: 19, marginLeft: 14, marginTop: 15}}
                source={require('../../assets/passwword.png')}
              />
            </View>
            <View style={{marginLeft: 9, marginTop: 8}}>
              <Text
                style={{
                  fontSize: 10,
                  color: 'white',
                }}>
                Password
              </Text>
              <TextInput
                style={styles.inputText}
                onChangeText={setPassword}
                placeholder="yourpassword"
                value={password}
              />
            </View>
          </View>
        </View>
        {/* submit part */}
        <View
          style={{
            width: 200,
            alignSelf: 'center',
            marginTop: 20,
            borderRadius: 15,
          }}>
          <Button title="LOGIN" onPress={() => signIn({email})} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
  },
  content: {
    flexDirection: 'column',
  },
  logo: {
    marginTop: 69,
    alignItems: 'center',
  },
  appName: {
    marginTop: 11,
    // backgroundColor: "red",
    alignItems: 'center',
  },
  login: {
    marginTop: 74,
    alignItems: 'center',
  },
  inputText: {
    fontSize: 12,
    color: 'white',
    width: 200,
  },
  email: {
    flexDirection: 'row',
    backgroundColor: '#5E5C09',
    height: 50,
    width: 247,
    borderRadius: 10,
  },
  password: {
    flexDirection: 'row',
    backgroundColor: '#5E5C09',
    marginTop: 18,
    height: 50,
    width: 247,
    borderRadius: 10,
  },
  submitButton: {
    marginTop: 41,
    height: 50,
    width: 247,
    backgroundColor: '#FBF8F1',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});
