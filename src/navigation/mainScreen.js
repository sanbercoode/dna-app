import * as React from 'react';

import HomeScreen from './screens/home';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, TouchableOpacity} from 'react-native';
import StatistikScreen from './screens/statistik';
import AboutScreen from './screens/about';

const Tab = createBottomTabNavigator();

export default function MainScreen() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          // position: 'absolute',
          // elevation: 0,
          // borderRadius: 15,
          // bottom: 10,
          // left: 20,
          // right: 20,
        },
        tabBarIcon: ({focused, color, size}) => {
          let iconSource;
          if (route.name === 'Home') {
            iconSource = focused
              ? require('../assets/menu.png')
              : require('../assets/menuBlur.png');
          } else if (route.name === 'Statistik') {
            iconSource = focused
              ? require('../assets/statistik.png')
              : require('../assets/statistikBlur.png');
          } else if (route.name === 'About') {
            iconSource = focused
              ? require('../assets/profile.png')
              : require('../assets/profileBlur.png');
          }

          return <Image source={iconSource} style={{height: 25, width: 25}} />;
        },
      })}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Statistik" component={StatistikScreen} />
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  );
}
