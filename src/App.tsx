import 'react-native-gesture-handler';

import SplashScreen from 'react-native-splash-screen'
import RootContext from './context/RootContext';
import { AuthProvider } from './context/AuthContext';
import { useEffect } from 'react';

function App() {
  useEffect(()=>{
    SplashScreen.hide()
  },[])
  return (
    <AuthProvider>
      <RootContext />
    </AuthProvider>
  );
}

export default App;
